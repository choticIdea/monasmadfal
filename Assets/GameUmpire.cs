﻿using UnityEngine;
using System.Collections;

public class GameUmpire : MonoBehaviour {

	// Use this for initialization
	public GameObject[] panah;
	public string[] teksPanah;
	public GameObject player;
	public GameObject canvasLoc;
	public GameObject infoCanvas;
	public GameObject menuCanvas;
	public GameObject creditCanvas;
	public GameObject minigameCanvas;
	public GameObject startPos;
	public UnityEngine.UI.Image fadeScreen;
	public GameObject head;
	public GameObject soundManager;
	public GameObject dirArrow;
	public GameObject[] mainMenuUI;
	public GameObject menuPos;
	// for displaying Monas information
	public UnityEngine.UI.Image textBG;
	public UnityEngine.UI.Text infoText;
	public UnityEngine.UI.Button infoButton;
	public bool testMode = true;
	bool gameStart = true;
	//buat penunjuk arah
	GameObject targetPanah;
	Vector2 curPos;
	Vector2 lastPos;
	Vector2 dis;
	Vector2 targetPos;
	Vector2 scale;
	Vector3 tempEulerrot;
	public int panahIndex = 0;
	void Start () {
		player.GetComponent<GvrViewer> ().VRModeEnabled = Main.vrMode;
		player.transform.position = menuPos.transform.position;
		player.transform.rotation = menuPos.transform.rotation;

		//player.transform.FindChild ("Head").rotation = menuPos.transform.rotation;
		Main.loadScore();
		scale = new Vector2 (3, 3);
		if (!testMode)
		player.GetComponent<Autowalk> ().canWalk = false;
		player.GetComponent<GvrViewer> ().VRModeEnabled = Main.vrMode;
		fadeScreen.enabled = true;
		fadeScreen.color = Color.black;
		StartCoroutine ("_fadeOut");
		dirArrow.SetActive (false);
		targetPanah = panah [panahIndex];
		curPos = new Vector2 (player.transform.position.x, player.transform.position.z);
		lastPos = curPos;
		tempEulerrot = transform.rotation.eulerAngles;
		targetPos = new Vector2(targetPanah.transform.position.x,targetPanah.transform.position.z);
	
		//menuCanvas.SetActive (true);
	}
	//Prototype arrow dirrection ala Skyrim, tinggal taruh objek panah diatas UI dan voila :D
	void changeDirection(){
		if ((lastPos - curPos).magnitude == 0)
			return;
		dis = curPos - targetPos;
		dis.Normalize ();
		dis.Scale(scale);

		Debug.Log (dis);
		Vector3 pos = new Vector3 (dis.x, dirArrow.transform.localPosition.y, dis.y);
		tempEulerrot = dirArrow.transform.localEulerAngles;
		tempEulerrot.y = Mathf.Rad2Deg * Mathf.Atan2 (dis.y, dis.x);
		Quaternion rot = Quaternion.Euler (tempEulerrot);

		dirArrow.transform.localRotation = rot;
			//pos = transform.position + pos;
		dirArrow.transform.localPosition = pos;
	}
	public void sceneTransition(){
		fadeScreen.enabled = true;
		StartCoroutine ("_fadeIn");
	}
	public void quit(){
		Application.Quit ();
	}
	public void showCredit(){
		for (int i = 0; i < mainMenuUI.Length; i++)
			mainMenuUI [i].SetActive (false);
		creditCanvas.SetActive (true);
	}
	public void showMenu(){
		for (int i = 0; i < mainMenuUI.Length; i++)
			mainMenuUI [i].SetActive (true);
		creditCanvas.SetActive (false);
	}
	IEnumerator  _fadeOut(){
		
		fadeScreen.color = Color.Lerp(fadeScreen.color,Color.clear,0.03f);
		yield return new WaitForSeconds (0.02f);

		if (fadeScreen.color.a <= 0.2) {
		//load scene and stuff
			//if the game is just starting or transitioned from mode select screen
			//activate Menu
			if (gameStart) {
				gameStart = false;
				soundManager.GetComponent<SoundManager> ().playBGM();
			}
			fadeScreen.enabled = false;
		} else {
			StartCoroutine ("_fadeOut");
		}
	}


	IEnumerator _fadeIn(){
		fadeScreen.color = Color.Lerp (fadeScreen.color, Color.black, 0.03f);
		yield return new WaitForSeconds (0.02f);
		if (fadeScreen.color.a >= 0.9) {
		//Warning lava code, should be fixed ASAP !!
			if (!gameStart) {
				player.transform.position = startPos.transform.position;
				player.GetComponent<Autowalk> ().canWalk = true;
				fadeScreen.color = Color.black;
				soundManager.GetComponent<SoundManager> ().setBGMVolume(0.4f);
				soundManager.GetComponent<SoundManager> ().playAmbience();
			} 
			StartCoroutine ("_fadeOut");
		} else {
			StartCoroutine ("_fadeIn");
		}
	}
	void Update () {
		curPos = new Vector2(player.transform.position.x,player.transform.position.z);
		changeDirection ();
	}
	public void setUI(){
		Vector3 pos =  canvasLoc.transform.position;
		pos.y = infoCanvas.transform.position.y;
		infoCanvas.transform.position = pos;
		Quaternion rot = head.transform.rotation;
		rot.eulerAngles = new Vector3 (0, rot.eulerAngles.y, 0);
		infoCanvas.transform.rotation = rot;

	}
	public void switchPanah(){
		if (panahIndex > panah.Length - 1)
			return;
		panah [panahIndex].SetActive(false);

		//UI settings
		player.GetComponent<Autowalk> ().canWalk = false;
		setUI ();
		textBG.gameObject.SetActive (true);
		infoButton.gameObject.SetActive (true);
		infoText.text = teksPanah [panahIndex];
		infoText.gameObject.SetActive (true);
		//end
		panahIndex++;
		targetPanah = panah [panahIndex];
		panah [panahIndex].SetActive(true);
		targetPos = new Vector2(targetPanah.transform.position.x,targetPanah.transform.position.z);
	}
	public void finishReadInfo(){
		player.GetComponent<Autowalk> ().canWalk = true;
		textBG.gameObject.SetActive (false);
		infoButton.gameObject.SetActive (false);
		infoText.gameObject.SetActive (false);
	}
	public void startGame(){
		Debug.Log ("wut?");
		sceneTransition ();
		dirArrow.SetActive (true);
	}
}
