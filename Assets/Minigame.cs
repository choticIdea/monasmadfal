﻿using UnityEngine;
using System.Collections;

public class Minigame : MonoBehaviour {

	// for displaying either player want to play minigame or not
	public GameObject[] uiComponent; 
	//public GameObject uiLoc;
	public GameObject manager;
	public GameObject player;
	public GameObject minigameStartLocation;
	public GameObject trashManager;
	public GameObject minigameCanvas;
	public UnityEngine.UI.Text inventoryText;
	public UnityEngine.UI.Text scoreText;
	public UnityEngine.UI.Text timeText;
	public UnityEngine.UI.Image fadeScreen;
	public int inventory=0;
	int MAX_INVENTORY = 10;
	public int time = 60;//minigame Timer in seconds
	int _time = 0;
	int elapsed = 0;
	public GameObject canvas;
	public GameObject resultBoard;
	bool minigameOver = false;
	bool insideMinigame = false;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		

	}
	IEnumerator decreaseTime(){
		
		yield return new WaitForSeconds (1f);
		elapsed++;
		_time--;
		timeText.text = _time+"";
		if (elapsed >= 20 && time > 0) {
			trashManager.GetComponent<TrashGenerator> ().generateTrash ();
			elapsed = 0;
			Debug.Log ("New Wave");
		}
		if (_time <= 0) {
			fadeScreen.enabled = true;
			fadeScreen.color = Color.black;
			Debug.Log ("Minigame over");
			minigameCanvas.SetActive (false);
			resultBoard.SetActive (true);
			resultBoard.GetComponent<ResultBoard> ().showResult ();
			minigameOver = true;
			StartCoroutine ("_fadeIn");
			player.GetComponent<Autowalk> ().canWalk = false;

		} else {
			StartCoroutine ("decreaseTime");
		}
	}
	public void addInventory(string sampah){
	    if (inventory >= MAX_INVENTORY)
			//play sound here
			return;
		sampah = sampah.ToLower();
		switch(sampah){
		case "kaleng":Main.kaleng += 1;
			break;
		case "botol" : Main.botol += 1;
			break;
		case "plastik" : Main.plastik += 1;
			break;
		case "kayu" :Main.kayu += 1;
			break; 
		case "kertas" : Main.kertas += 1;
			break; 					      	
		}
		inventory++;
		inventoryText.text = inventory + "/" + MAX_INVENTORY;

	}
	public void showUI(){
		canvas.SetActive (true);
	}

	public void hideUI(){
		canvas.SetActive (false);
		player.GetComponent<Autowalk> ().canWalk = true;

	
	}
	IEnumerator  _fadeOut(){

		fadeScreen.color = Color.Lerp(fadeScreen.color,Color.clear,0.03f);
		yield return new WaitForSeconds (0.02f);
		if (fadeScreen.color.a <= 0.2) {
			//load scene and stuff
			//if the game is just starting or transitioned from mode select screen
			//activate Menu
			fadeScreen.enabled = false;
			if (!minigameOver) {
				
				player.GetComponent<Autowalk> ().canWalk = true;
				trashManager.SetActive (true);
				trashManager.GetComponent<TrashGenerator> ().generateTrash ();
				timeText.text = time + "";
				StartCoroutine ("decreaseTime");
			} else {
				
				//player.transform.rotation = minigameStartLocation.transform.rotation;
				if(insideMinigame)
				player.GetComponent<Autowalk> ().canWalk = false;
				else
					player.GetComponent<Autowalk> ().canWalk = true;

			}
			
		} else {
			StartCoroutine ("_fadeOut");
		}
	}
	IEnumerator _fadeIn(){
		fadeScreen.color = Color.Lerp (fadeScreen.color, Color.black, 0.03f);
		yield return new WaitForSeconds (0.02f);
		if (fadeScreen.color.a >= 0.9) {
			//Warning lava code, should be fixed ASAP !!
			if(!minigameOver)
			minigameCanvas.SetActive (true);
			if (insideMinigame) {
				player.transform.position = minigameStartLocation.transform.position;
				player.transform.rotation = minigameStartLocation.transform.rotation;
			}
			else if (!insideMinigame) {
				player.transform.position = this.transform.position;
			}
			StartCoroutine ("_fadeOut");
		} else {
			StartCoroutine ("_fadeIn");
		}
	}
	public void startMinigame(){
		Main.playerScore = 0;
		minigameOver = false;
		insideMinigame = true;
		fadeScreen.enabled = true;
		fadeScreen.color = Color.black;
		resultBoard.SetActive (false);
		StartCoroutine ("_fadeIn");
		_time = time;
	}
	public void backToMainGame(){
		player.GetComponent<Autowalk> ().canWalk = false;
		insideMinigame = false;
		fadeScreen.enabled = true;
		fadeScreen.color = Color.black;
		StartCoroutine ("_fadeIn");
	}
	void OnTriggerEnter(Collider c){
	/*	if (c.tag == "Player") {
			manager.GetComponent<GameUmpire> ().setUI ();
			showUI ();
			GetComponent<BoxCollider> ().enabled = false;
			player.GetComponent<Autowalk> ().canWalk = false;
       deprecated
		}*/


	}
	public void throwTrash(){
		
		Main.playerScore += inventory;
		scoreText.text = Main.playerScore+"";
		inventory = 0;
		inventoryText.text = inventory + "/" + MAX_INVENTORY;
	}
}
