﻿using UnityEngine;
using System.Collections;

public class TrashGenerator : MonoBehaviour {

	// Use this for initialization
	public GameObject[] trashes;
	public GameObject[] trashLocations;
	int[] locUsed;// for marking which locations has trash already, 1 for yes, 0 for no.
	//The index here is referring to trashLocations index;
	public int maxTrash = 20;
	int trashCount = 0;
	int trashLoc = 0;


	void Start () {
		
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void generateTrash(){
		locUsed = new int[trashLocations.Length];
		trashCount = 0;
		while (trashCount < maxTrash) {
			if (Random.value >= 0.5f) {
				if (locUsed [trashLoc] == 1)
					continue;
				locUsed [trashLoc] = 1;
				Instantiate (trashes [Random.Range (0, 4)],trashLocations[trashLoc].transform.position,this.transform.rotation);	
				trashCount += 1;
			}
			trashLoc++;
			if (trashLoc >= trashLocations.Length)
				trashLoc = 0;
		}
	}
}
