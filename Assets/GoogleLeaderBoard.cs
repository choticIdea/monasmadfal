﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.OurUtils;
public class GoogleLeaderBoard : MonoBehaviour
{
	
	public string leaderboard;
	public static int attempt = 0;
	public static int maxAttempt = 1;

	void Start ()
	{
		// recommended for debugging:
		leaderboard = LeaderboardManager.leaderboard_highscore;

		// Activate the Google Play Games platform

	}

	/// <summary>
	/// Login In Into Your Google+ Account
	/// </summary>
	public static void LogIn ()
	{
		
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate ();
		Social.localUser.Authenticate ((bool success) =>
			{
				if (success) {
					
					Debug.Log ("Login Sucess");
				} else {
					if(attempt < maxAttempt)
					{
						attempt++;
						LogIn();
					}else{
						attempt = 0;
						return;
					}
					Debug.Log ("Login failed");
				}
			});
	}

	/// <summary>
	/// Shows All Available Leaderborad
	/// </summary>
	public void OnShowLeaderBoard ()
	{
		//        Social.ShowLeaderboardUI (); // Show all leaderboard
		Debug.Log(PlayGamesPlatform.Instance);
		PlayGamesPlatform.Instance.ShowLeaderboardUI (leaderboard); // Show current (Active) leaderboard
	}
	/// <summary>
	/// Adds Score To leader board
	/// </summary>
	public void OnAddScoreToLeaderBorad ()
	{
		if (Social.localUser.authenticated) {
			Social.ReportScore (Main.playerScore, leaderboard, (bool success) =>
				{
					if (success) {
						Debug.Log ("Update Score Success");

					} else {
						Debug.Log ("Update Score Fail");
					}
				});
		}
	}
	/// <summary>
	/// On Logout of your Google+ Account
	/// </summary>
	public void OnLogOut ()
	{
		((PlayGamesPlatform)Social.Active).SignOut ();
	}

}
