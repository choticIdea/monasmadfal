﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
public class MadfalS : MonoBehaviour
{
	public GameObject madfal;
	public GameObject lenovo;
	public GameObject pilihMode;
	public UnityEngine.UI.Image fadeScreen;
	// Use this for initialization
	void Awake ()
	{
		GoogleLeaderBoard.LogIn ();
		Advertisement.Initialize ("1099375", false);
		StartCoroutine (waitForAnimMadfal ());
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	IEnumerator _fadeIn(){
		fadeScreen.color = Color.Lerp (fadeScreen.color, Color.black, 0.03f);
		yield return new WaitForSeconds (0.02f);
		if (fadeScreen.color.a >= 0.9f) {
			fadeScreen.color = Color.black;
			SceneManager.LoadSceneAsync("play");
		} else {
			StartCoroutine ("_fadeIn");
		}
	}
	IEnumerator waitForAnimMadfal ()
	{
		yield return new WaitForSeconds (8f);
		madfal.SetActive(false);
		lenovo.SetActive(true);
		lenovo.GetComponent<Animator>().SetTrigger("PlayAnim");
		StartCoroutine (waitForAnimLenovo ());
		yield break;
	}
	IEnumerator showAds(){
		while (!Advertisement.IsReady())
			yield return null;
		Advertisement.Show ();
	}
	IEnumerator waitForAnimLenovo()
	{
		yield return new WaitForSeconds (5f);

		pilihMode.SetActive(true);
		yield break;
	}

	public void klikMode(string nama)
	{
		//sound
	//	SoundManager.playSound("CLICK");
		if(nama == "vr")
		{
			Main.vrMode = true;
		}
		else if(nama == "normal")
		{
			Main.vrMode = false;
		}
		fadeScreen.enabled = true;
		StartCoroutine("showAds");
		StartCoroutine("_fadeIn");
		//SoundManager.playSound("BGM");
		//ads
		//AdmobManager.instance.requestAds();

	}

}
