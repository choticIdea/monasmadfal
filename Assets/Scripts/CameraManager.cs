﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	public Camera cameraAwal;
	public Camera cameraVR;
	public GameObject hero;
	public GameObject cardBoardMain;

	private bool isActive = false;

	// Use this for initialization
	void Start () {
		cameraAwal.enabled = true;
		hero.GetComponent<Autowalk>().enabled = false;
		hero.GetComponent<GvrViewer>().enabled = false;
		hero.SetActive(false);
		cameraVR.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)){
			if(isActive == false){
				cameraAwal.enabled = false;
				hero.GetComponent<Autowalk>().enabled = true;
				hero.GetComponent<GvrViewer>().enabled = true;
				hero.SetActive(true);
				cameraVR.enabled = true;
			}
		}
	}
}
