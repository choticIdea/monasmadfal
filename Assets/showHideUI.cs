﻿using UnityEngine;
using System.Collections;

public class showHideUI : MonoBehaviour {

	// Use this for initialization
	public GameObject[] buttons;
	bool toggle = false;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void showHide(){
		if (!toggle) {
			toggle = true;
			for (int i = 0; i < buttons.Length; i++)
				buttons [i].SetActive (true);
			
		} else if (toggle) {
			toggle = false;
			for (int i = 0; i < buttons.Length; i++)
				buttons [i].SetActive (false);
			
		}
	}

}
