﻿using UnityEngine;
using System.Collections;

public class Sampah : MonoBehaviour {

	// Use this for initialization
	AudioClip throwed;// sfx for trashes
	public UnityEngine.UI.Text scoreField;
	public GameObject minigameManager;
	Minigame minigameScript;
	public GameObject soundManager;
	public string type;
	void Start () {
		minigameScript = minigameManager.GetComponent<Minigame> ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	// for putting away thrash and update score
	public void takeTrash(){
		//play sfx
		//update game score
		//Main.playerScore += 10;
		//scoreField.text = "" + Main.playerScore;

	}
	void OnTriggerEnter(Collider c){
		if(c.tag == "Player"){
			soundManager.GetComponent<SoundManager> ().playTrash ();
		minigameScript.addInventory(type);
		Destroy (this.gameObject);
		Destroy (this.transform.parent.gameObject);
			}
	}
}
