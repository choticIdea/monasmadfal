﻿using UnityEngine;
using System.Collections;

public class TrashCan : MonoBehaviour {

	public GameObject minigameManager;
	public GameObject soundManager;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider c){
		if (c.tag == "Player") {
			minigameManager.GetComponent<Minigame> ().throwTrash ();
			soundManager.GetComponent<SoundManager> ().playTrash ();

		}
	}
}
