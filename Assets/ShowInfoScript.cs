﻿using UnityEngine;
using System.Collections;

public class ShowInfoScript : MonoBehaviour {

	// Use this for initialization
	public UnityEngine.UI.Text textField;
	public string text;
	public UnityEngine.UI.Text timeField;
	public bool useTimer = true;
	public float time = 2;
	public void showText(){
		if (useTimer) {
			Debug.Log ("timed!");
			StartCoroutine ("timer");

		} else {
			Debug.Log (" not timed!");
			textField.text = text;
		}
	}
	public void hideText(){
		textField.text = "";
	}

	IEnumerator timer(){

		timeField.text = "Menampilkan informasi..";
		yield return new WaitForSeconds (time);
		textField.text = text;
		timeField.text = "";
		Debug.Log ("Wait");
	}

}
