﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ResultBoard : MonoBehaviour {

	public Text kayuText;
	public Text kertasText;
	public Text plastikText;
	public Text kalengText;
	public Text botolText;

	public Text bestText;
	public Text currentScoreText;
	public void showResult(){
		currentScoreText.text = "" + Main.playerScore;
		kayuText.text = "Kayu : " + Main.kayu;
		kertasText.text = "Kertas : " + Main.kertas;
		plastikText.text= "Plastik : " + Main.plastik;
		botolText.text= "Botol : " + Main.botol;
		kalengText.text= "Kaleng : " + Main.kaleng;
		bestText.text = Main.bestScore+" ";
		/*Compare player score here, if bigger than before save it and show it under bestText and currentScore
		 *  dont forget to modify the best score in Main script
		 */
		if (Main.bestScore < Main.playerScore) {
			Main.bestScore = Main.playerScore;
			Main.saveScore ();
		}
	}
}
