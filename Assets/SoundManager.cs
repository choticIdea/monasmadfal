﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	// Use this for initialization
	public AudioClip clickSfx;
	public AudioClip bestResultSfx;
	public AudioClip trashSfx;

	public void playAmbience(){
		GetComponents<AudioSource> () [1].Play();
	}
	public void playFootSteps(){
		GetComponents<AudioSource> () [2].Play();
	}
	public void stopFootSteps(){
		GetComponents<AudioSource> () [2].Stop ();
	}
	public void setBGMVolume(float vol){
		GetComponents<AudioSource> () [0].volume = vol;
	}
	public void playBGM(){
		GetComponents<AudioSource> () [0].Play();
	}
	public void playClick(){
		GetComponents<AudioSource> () [3].clip = clickSfx;
		GetComponents<AudioSource> () [3].Play ();
	}
	public void playTrash(){
		GetComponents<AudioSource> () [3].clip = trashSfx;
			GetComponents<AudioSource> () [3].Play ();
		}
	public void playbestResult(){
		GetComponents<AudioSource> () [3].clip = bestResultSfx;
			GetComponents<AudioSource> () [3].Play ();
		}

}
