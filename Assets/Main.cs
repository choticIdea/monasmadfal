﻿using UnityEngine;
using System.Collections;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Main  {

	// Use this for initialization

	
	// Update is called once per frame
	public static int playerScore = 0;
	public static int bestScore = 0;
	public static int kaleng = 0;
	public static int botol = 0;
	public static int plastik = 0;
	public static int kayu = 0;
	public static int kertas = 0;
	public static bool vrMode = false; // true for activating VR mode, false for normal;

	public static void saveScore(){
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/savedScore.gd");
		bf.Serialize(file, bestScore);
		file.Close();
	}
	public static void loadScore(){
		if(File.Exists(Application.persistentDataPath + "/savedScore.gd")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedScore.gd", FileMode.Open);
			bestScore = (int)bf.Deserialize (file);
			file.Close();
		}
	}
}
